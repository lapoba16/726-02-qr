CC=g++-6    #  OSX with homebrew g++/gcc
CC=g++
CFLAG=-fopenmp -O3 -g#-pg

#=============================

PROGS = qrOnly \
   makeIn \
   testIn \
   etime.o \
   io.o \
   matrix.o \
   qr.o \
   tikhonov.o \

all: ${PROGS}

#=============================

run: qrOnly
	cp qrOnly run

qrOnly: etime.o io.o matrix.o qr.o Src/test.cpp tikhonov.o
	$(CC) -o $@ $(CFLAG) etime.o io.o matrix.o qr.o tikhonov.o Src/test.cpp $(CFLAG)
makeIn: makeInput/core.cpp io.o
	$(CC) -o $@ $(CFLAG) makeInput/core.cpp io.o $(CFLAG)
testIn: makeInput/echo.cpp io.o
	$(CC) -o $@ $(CFLAG) makeInput/echo.cpp io.o $(CFLAG)

etime.o: Src/etime.*
	$(CC) -c $(CFLAG) Src/etime.cpp $(CFLAG)
io.o: Src/io.*
	$(CC) -c $(CFLAG) Src/io.cpp $(CFLAG)
matrix.o: Src/matrix.* etime.o
	$(CC) -c $(CFLAG) Src/matrix.cpp $(CFLAG)
qr.o: Src/qr.* matrix.o
	$(CC) -c $(CFLAG) Src/qr.cpp $(CFLAG)
tikhonov.o: Src/tikhonov.*
	$(CC) -c $(CFLAG) Src/tikhonov.cpp $(CFLAG)

#=============================

clean:
	rm -rf *.o ${PROGS} *.out #my_*
