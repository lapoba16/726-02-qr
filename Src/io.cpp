#include "io.h"

double* readIn(const char* filename, int** sizes) {
   FILE *file = NULL;
   file = fopen(filename, "rb");
   if(!file || file==NULL) {
      printf("Could not open %s\n", filename);
      return NULL;
   }
   int m, n;
   int tr;
   tr = fread(&m,sizeof(int),1,file);
   tr = fread(&n,sizeof(int),1,file);
   if(*sizes==NULL)
      *sizes=(int*)malloc(sizeof(int)*2);
   else
      *sizes=(int*)realloc(*sizes, 2*sizeof(int));
   (*sizes)[0] = m;
   (*sizes)[1] = n;
   double *ptr = (double*)malloc(sizeof(double)*m*n);
   tr = fread(ptr, sizeof(double), m*n, file);
   fclose(file);
   return ptr;
}

int printOut(double* data, const char* filename, int* len) { 
   FILE *file = fopen(filename, "wb");
   int ret = 0;
   if(!file) {
      printf("Could not open to write\n");
      return -1;
   }
   if(data!=NULL) {
      fwrite(len, sizeof(int), 2, file);
      fwrite(data, sizeof(double), len[0]*len[1], file);
   } 
   else
      ret = -1;
   fclose(file);
   return ret;
}

void cleanQuit() {
   fclose(stdin);
   fclose(stdout);
   fclose(stderr);
   exit(0);
   return;
}
