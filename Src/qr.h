#pragma once
#include "matrix.h"
#include <unistd.h>

/*
 * int QR(Matrix *A, Matrix *R, int t);
 * Description: Given a nonempty Matrix A,
 *    compute Matrices Q and R such that
 *    A = Q*R. A will become Q, R will be
 *    new or overwritten. A will be
 *    overwritten with Q.
 *    QR Will create a thread team of
 *    t threads, but will only use the
 *    master thread.
 * Pre: A is not NULL and is not empty. A
 *    and R are overwritten and R is overwritten
 *    if it exists, created in heap if not.
 * Post: Q and R exist. A is overwritten.
 */
int QR(Matrix *, Matrix *, int);
