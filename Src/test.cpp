#include "io.h"
#include "qr.h"
#include "tikhonov.h"

#ifndef BEL_DEBUG
#define BEL_DEBUG
#endif

using namespace std;

int main(int argc, char** argv) {
   atexit(cleanQuit);
   int thread_cnt = (argc>1) ? atoi(argv[1]) : 1;
   if(thread_cnt<1)
      thread_cnt = 1;
   printf("Threads = %i, ", thread_cnt);
   int sz[2];
   double *data = NULL;
   double alpha = (argc>4) ? atof(argv[4]) : 0.0;
   if(alpha<0 || alpha>1)
      alpha = 0;
   printf("Alpha = %lf, ", alpha);
   string fname = (argc>2) ? argv[2] : "IO/h_le.dat";
   printf("H file = %s, ", fname.c_str());
   // read in A(H) and b(g)
   matrix_tikon(fname.c_str(), &(sz[0]), &(sz[1]), alpha, &data);
   Matrix A(data, sz);
   free(data);
   fname = (argc>3) ? argv[3] : "IO/g_le.dat";
   printf("G file = %s\n", fname.c_str());
   vector_tikon(fname.c_str(), &(sz[0]), &(sz[1]), alpha, &data);
   Matrix b(data, sz);
   free(data);

   tic();
   // QR on A(H)
   Matrix Q, R;
   Q = A;
   QR(&Q, &R, thread_cnt);
   toc();
   printf("Parallel completed in %.2f seconds.\n", etime());
   // d = Qt b(g)
   Q.transpose();
   Matrix d = Q * b;
   // Back sub R x = d
   Matrix x = backSubstitute(&R, &d);
   // is x == f?
   toc();
   printf("Completed in %.2f seconds.\n", etime());

   sz[0] = x.getRows();
   sz[1] = x.getCols();
   fname = (argc>5) ? argv[5] : "my_out";
   printOut(&(x[0]), fname.c_str(), sz);

   A.clean();
   b.clean();
   Q.clean();
   R.clean();
   d.clean();
   x.clean();
   exit(0);
   return -1;
}
