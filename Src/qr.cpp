#include "qr.h"

int QR(Matrix *origA, Matrix *origR, int t) {
   if(origA==NULL || origA==0)
      return -1;
   Matrix A(0,0);
   A = *origA; 
   Matrix R(A.getCols(), A.getCols(), 0);
   Matrix R12;
   Matrix qc = A.extract(0, 'c');;
   int ACols = A.getCols();
   int ARows = A.getRows();
   int RCols = R.getCols();
   float ttl = ACols * ACols / 2 + (ARows - ACols) * ACols;
   float done;
   #pragma omp parallel num_threads(t)
   {
      Matrix q, qb;
      Matrix R12b;
      int i, j, k;
      int start, stop;
      double r11;
      #pragma omp master
         R12.resize(1, ACols-1, 0);
      #pragma omp barrier

      // for each column in A
      for(i=0; i<ACols; i++) {
         q = qc;
         r11 = q.length();
         q *= (1.0/r11);
         #pragma omp for schedule(dynamic)
         for(j=0; j<q.getRows(); j++)
            A[i*ARows+j] = q[j];
         q.transpose();
         #pragma omp master
            if(i%10==0) {
               done = i * i / 2.0 + i * (ARows - i);
               printf("\r%6.2f%%", 100.0*done/ttl);
               fflush(stdout);
            }
         R[i*RCols+i] = r11;
         R12.cropMul(&q, &A, 0, i+1, omp_get_thread_num(), t);
         q.transpose();
         qb = q;
         #pragma omp barrier
         R12b = R12;
         // Set R's (new) first row = R12
         #pragma omp for schedule(dynamic)
         for(j=i+1; j<RCols; j++)  
            R[j*R.getRows() + i] = R12b[j - i - 1];

         // Remaining A -= q * row(R12)
         if(i+1<ACols) {
            #pragma omp for schedule(dynamic)
            for(j=0; j<q.getHeight(); j++) {
               for(k=0; k<R12b.getWidth(); k++) {
                  A[j + (i+k+1) * ARows] -= qb[j] * R12b[k];
               }
               qc[j] = A[j + (i+1)*ARows];
            }
            //#pragma omp barrier
            // technically this isn't safe, but
            // each thread would also have to pass
            // both of the above loops, one nested,
            // and get here before another thread
            // does the first statement after the
            // barrier (R12b = R12 finishes AFTER
            // a thread does the loop and nested
            // loop and the following). Pretty safe
            // gamble if you ask me.
            #pragma omp master
               R12.resize(1, ACols-(i+2), 0);
            #pragma omp barrier
         }
      }
   }
   *origR = R;
   *origA = A;
   printf("\r%6.2f%%\n", 100.0);
   return 0;
}












