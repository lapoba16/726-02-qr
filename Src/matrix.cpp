#include "matrix.h"

// Constructors
Matrix::Matrix() {
   data = 0;
   h = w = 0;
}
Matrix::Matrix(const Matrix &a) {
   h = a.h;
   w = a.w;
   data = new double[h*w];
   memcpy(data, a.data, sizeof(double)*w*h);
}
Matrix::Matrix(double *predata, int *sz) {
   //data = predata;
   h = sz[0];
   w = sz[1];
   data = new double[h*w];
   memcpy(data, predata, sizeof(double)*w*h);
}
Matrix::~Matrix() {
   clean();
}
Matrix::Matrix(int hi, int wi, double a) {
   data = 0;
   h = w = 0;
   if(hi!=0 && wi!=0)
      resize(hi, wi, a);
}
// Get
double Matrix::length() const{
   // either a single row or a single column,
   // else return 0
   double sum = 0;
   if(w==1 || h==1) {
      for(int i=0; i<h; i++) {
         for(int j=0; j<w; j++) {
            sum += data[i*w+j] * data[i*w+j];
         }
      }
   }
   return sqrt(sum);
}
// Set
bool Matrix::resize(int hi, int wi, double a) {
   clean();
   if(hi!=0 && wi!=0) {
      h = hi;
      w = wi;
      data = new double[h*w];
      fill(a);
   }
   return (data!=NULL && data!=0);
}
bool Matrix::fill(double a) {
   if(data==NULL || data==0)
      return false;
   std::fill_n(data, h*w, a);
   return true;
}
void Matrix::clean() {
   if((data!=NULL && data!=0)) {
      delete[] data;
      data = 0;
   } 
   h = w = 0;
}
// Operator Overloads
Matrix Matrix::operator+(const Matrix a) const{
   Matrix c;
   if(a.h!=h || a.w!=w || 
      data==NULL || data==0 || 
      a.data==NULL || a.data==0
   )
      return c;
   c.resize(a.h, a.w);
   for(int i=0; i<h*w; i++) {
      c.data[i]= this->data[i] + a.data[i];
   }
   return c;
}
Matrix Matrix::operator-(const Matrix a) const{
   Matrix c;
   if(a.h!=h || a.w!=w || 
      data==NULL || data==0 || 
      a.data==NULL || a.data==0
   )
      return c;
   c.resize(a.h, a.w);
   for(int i=0; i<h*w; i++)
      c.data[i] = this->data[i]- a.data[i];
   return c;
}
Matrix Matrix::operator*(const Matrix a) const{
   Matrix c;
   if(data==NULL || data==0 || 
      a.data==NULL || a.data==0
   )
      return c;
   // If vectors, return cross product in a 1x1 matrix
   if(w==1 && a.w==1 && h==a.h) {
      c.resize(1);
      for(int i=0; i<h; i++) {
         c.data[0] += data[i]*a.data[i];   
      }
      return c;
   }
   /* // regularization kills this, forget it
   if(w!=a.h)
      return c;
   */
   // Else multiply correctly-sized matrices
   c.resize(h, a.w);
   double tmp;
   for(int i=0; i<c.h; i++) 
      for(int j=0; j<c.w; j++) {
         tmp = 0.0;
         for(int k=0; k<a.h; k++) {
            tmp += data[k*h+i] * a.data[j*a.h+k];
         }
         c.data[j*c.h+i] = tmp;
      }
   return c;
}
Matrix Matrix::operator*(const double a) const{
   Matrix c;
   if(data==0 || data==NULL)
      return c;
   c.resize(h,w);
   for(int i=0; i<h*w; i++)
      c.data[i] = data[i] * a;
   return c;
}
Matrix& Matrix::operator*=(const double a) {
   if(data==0 || data==NULL)
      return *this;
   for(int i=0; i<h*w; i++)
      data[i] *= a;
   return *this;
}
Matrix& Matrix::operator=(const Matrix &a) {
   if(this!=&a) {
      clean();
      w = a.w;
      h = a.h;
      data = new double[w*h];
      memcpy(data, a.data, sizeof(double)*w*h);
   }
   return *this;
}
ostream& operator<<(ostream &out, const Matrix &a) {
   if(a.data==NULL || a.data==0)
      return out;
   char buff[15];
   out << " _ ";
   for(int i=0; i<7*a.w; i++)
      out << " ";
   out << "_\n";
   for(int i=0; i<a.h; i++) {
      out << ((i==a.h-1)? "|_ " : "|  ");
      for(int j=0; j<a.w; j++) {
         sprintf(buff, "%6.2f", a.data[j*a.h + i]);
         out << buff
            << ((j==a.w-1)?" ":"")
            << ((i==a.h-1 && j==a.w-1)?"_":" ");
      }
      out << "|\n";
   }
   return out;
}
double& Matrix::operator[](int index) {
   return data[index];
}

Matrix Matrix::extract(int tier, char selection, int secondary) {
   Matrix c(h-secondary, 1, 0);   
   if(selection=='r') {
      c.resize(1, w-secondary, 0);
      for(int i=secondary; i<w; i++) {
         c.data[i-secondary] = data[i*w+tier];
      }
   }
   else {
      for(int i=secondary; i<h; i++) {
         c.data[i-secondary] = data[tier*h+i]; 
      }
   }
   return c;
}

int Matrix::cropMul(const Matrix *A, const Matrix *B, const int skipR, const int skipC, int myT, int totalT) {
   if(h!=A->h && w!=B->w-skipC) {
      fprintf(stderr
         , "Matrix::cropMul(): The caller is not prepared to accept "
      );
      fprintf(stderr
         , "[%i][%i] result, is [%i][%i]\n"
         , A->h, B->w-skipC
         , h, w
      );
      return -1;
   }
   if(B->h-skipR != A->w) {
      fprintf(stderr
         , "Matrix::cropMul(): Invalid input: "
      );
      fprintf(stderr
         , "A[%i][%i] * B[%i-%i][%i-%i].\n"
         , A->h, A->w
         , B->h, skipR, B->w, skipC
      );
      return -1;
   }
   int mystart=0, myend=w;
   if(totalT>1) 
      distributeWork(&mystart, &myend, myT, totalT, w);
   innerCropMul(mystart, myend, A, B, skipR, skipC);
   return 0;
}
void Matrix::innerCropMul(int a, int b, const Matrix *A, const Matrix *B, const int skipR, const int skipC) {
   double tmp;
   double minval = 0.0000000000001;
   double maxval = 
      1000.0 *
      1000.0 *
      1000.0 *
      1000.0 *
      1000.0 *
      1000.0 *
      1000.0 *
      1000.0 * 1000.0;
   double *Adata = A->data;
   double *Bdata = B->data;
   int Aw = A->w, Ah = A->h;
   int Bw = B->w, Bh = B->h;
   for(int i=0; i<h; i++) {
      for(int j=a; j<b; j++) {
         tmp = 0;  
         for(int k=0; k<Aw; k++) {
            tmp += Adata[k*Ah+i] * Bdata[(k+skipR)+(j+skipC)*Bh];
         }
         if( 
            (tmp>minval && tmp<maxval) 
            || (tmp<minval*-1 && tmp>maxval*-1) 
         )
            data[j*h+i] = tmp;
         else
            data[j*h+i] = 0;
         //data[j*h+i] = tmp;
      }
   }
}

void distributeWork(int *start, int *stop, int me, int p, int n) {
   int load;
   int modded = n%p;
   if(me < modded) {
      load = n/p + 1;
      *start = me * load;
   } else {
      load = n/p;
      *start = (load+1) * modded + (me-modded) * load;
   }
   *stop = *start + load;
}

void Matrix::transpose() {
   // if vec, just switch h&w
   if(h==1 || w==1) {
      int tmp = h;
      h = w;
      w = tmp;
      return;
   } else {
      innerTrans();
   }
} 
void Matrix::innerTrans() {
   Matrix c(w, h, 0);
   for(int i=0; i<c.w; i++)
      for(int j=0; j<c.h; j++)
         c[i*w+j] = data[j*h+i];
   *this = c;
}

Matrix backSubstitute(Matrix *A, Matrix *b) {
   Matrix c;
   bool transposed = false;
   // fix vect if necessary
   if(b->getWidth()>1) {
      transposed = true;
      b->transpose();
   }
   // if valid, solve
   if(A->getWidth()==b->getHeight()) {
      // assume A is upper triangular
      // dimensions are acceptable
      c.resize(b->getHeight());
      int cLen = c.getHeight();
      // for each row
      int i;
      for(i=cLen-1; i>=0; i--) {
         // b's value -= columns in A after i
         double tmp = (*b)[i];
         for(int j=i+1; j<A->getWidth(); j++) {
            tmp -= (*A)[i+j*A->getHeight()] * c[j];
         }
         // divide by main diagonal
         c[i] = tmp / (*A)[i+i*A->getHeight()];
      }
   }
   // else return nothing
   else
      c.resize(0);
   // put vect back if necessary
   if(transposed)
      b->transpose();
   return c;
}







