
#include <stdio.h>
#include <stdlib.h>

#include "tikhonov.h"

using namespace std;

/* ----------------------------------------------------------------- */
void matrix_tikon( const char * s, int * mp, int * np, 
                               double alpha, double ** dpp )
{
   int m, n ;
   double * dp ;
   double * dpk ;
   double * dpkx ;
   FILE * fp ;
   int bytes ;
   int mx, j, k ;
   int dummy ;

   fp = fopen ( s, "r" ) ;
   if (fp == NULL) {
     fprintf(stderr,"read_dat_with_Tikhonov(): Unable to open file '%s'\n",s );
     exit(19) ;
   }
   dummy = fread( &m, sizeof(int), 1, fp ) ;
   dummy = fread( &n, sizeof(int), 1, fp ) ;
   mx = m + n ;
   bytes = mx * n * sizeof(double) ;
   dp = (double *) malloc( bytes ) ;
   if (dp == NULL) {
     fprintf(stderr,"read_matix_with_Tikhonov(): malloc failed " ) ;
     fprintf(stderr,"for %d bytes on file '%s'\n", bytes, s ) ;
     exit(18) ;
   }
   for ( k = 0 ; k < n ; k++ ) {
      dpk = dp + k * mx ;
      dummy = fread( dpk, sizeof(double), m, fp ) ;
      dpkx = dpk + m ;
      for ( j = 0 ; j < n ; j++ ) {
         *(dpkx+j) = 0.0 ;
      }
      *(dpkx+k) = alpha ;
   }
   fclose (fp) ;
   *mp = mx ;  *np = n ;
   *dpp = dp ;   
}

/* ----------------------------------------------------------------- */
void vector_tikon( const char * s, int * mp, int * np, int nn,  
                                      double ** dpp )
{
   int m, n ;
   double * dp ;
   double * dpk ;
   double * dpkx ;
   FILE * fp ;
   int bytes ;
   int mx, i, j, k ;
   int dummy ;

   fp = fopen ( s, "r" ) ;
   if (fp == NULL) {
     fprintf(stderr, "read_extend_vector_for_Tikonov(): " ) ;
     fprintf(stderr, "Unable to open file '%s'\n",s );
     exit(17) ;
   }
   dummy = fread( &m, sizeof(int), 1, fp ) ;
   dummy = fread( &n, sizeof(int), 1, fp ) ;
   if ( n != 1 ) {
       fprintf(stderr,"read_extend_vector_for_Tikhonov():  File '%s' ", s ) ;
       fprintf(stderr," does not contain a column vector.\n" ) ;
       exit(44) ;
   }
   mx = m + nn ;
   bytes = mx * n * sizeof(double) ;
   dp = (double *) malloc( bytes ) ;
   if (dp == NULL) {
     fprintf(stderr,"read_extend_vector_for_Tikhonov(): malloc failed " ) ;
     fprintf(stderr,"for %d bytes on file '%s'\n", bytes, s ) ;
     exit(68) ;
   }
   dummy = fread( dp, sizeof(double), m, fp ) ;
   fclose (fp) ;
   for ( i = m ; i < mx ; i++ ) {
       *(dp + i) = 0.0 ;
   }
   *mp = mx ;  *np = n ;
   *dpp = dp ;   
}

