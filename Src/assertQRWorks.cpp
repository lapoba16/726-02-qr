#include "io.h"
#include "qr.h"

#ifndef BEL_DEBUG
//#define BEL_DEBUG
#endif

int main(int argc, char** argv) {
   int thread_cnt = (argc>1) ? atoi(argv[1]) : 1;
   int *sz = NULL;
   double *data = (argc>2) ? readIn(argv[2], &sz) : readIn("IO/inputA.dat", &sz);
   printf("Found file %i x %i\n", sz[0], sz[1]);
   Matrix A(data, sz);
   free(data);
   Matrix R;
   Matrix Q = A;

   #ifdef BEL_DEBUG
   cout<<"A\n"<<A<<endl;
   #endif

   tic();
   QR(&Q, &R, thread_cnt);
   toc();
   printf("\n%.2f seconds to QR()\n", etime());

   return cleanQuit();

   Matrix Qt = Q;
   tic();
   Qt.transpose();
   toc();
   printf("%.2f seconds to transpose()\n", etime());

   #ifdef BEL_DEBUG
   cout<<"Q\n"<<Q<<endl;
   cout<<"R\n"<<R<<endl;
   Matrix orthoTest = Q*R;
   cout<<"Q*R\n"<<orthoTest<<endl;
   #else
   Matrix orthoTest;
   #endif


   orthoTest.resize(Qt.getRows(), Q.getCols(), 0);
   tic();
   #pragma omp parallel num_threads(4)
   {
      orthoTest.cropMul(&Qt, &Q, 0, 0, omp_get_thread_num(), omp_get_num_threads());
   }
   toc();
   printf("%.2f seconds to orthoTest = Qt * Q\n", etime());
 
   for(int i=0; i<orthoTest.getRows(); i++) {
      if(i>0)
         cout<<"< "<<orthoTest[orthoTest.getRows()*i-orthoTest.getRows()+i];
      cout<<", "<<orthoTest[orthoTest.getRows()*i + i];
      if(i+1<orthoTest.getRows())
         cout<<", "<<orthoTest[orthoTest.getRows()*i+orthoTest.getRows()+i]<<" >";
      if(i>0 && i%10==0)
         cout<<endl;
   }

   return cleanQuit();
}
