#pragma once
#include <stdio.h>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <omp.h>
#include "etime.h"

using namespace std;

/*
 * class Matrix
 * Description: holds and performs basic
 *    matrix and vector operations via
 *    conventional operators.
 *    The matrix must be given a size,
 *    either in the constructor or
 *    via the resize() member,
 *    before it can be operated on.
 */
class Matrix {
private:
   double *data;
   short int h, w;
   /*
    * void innerTrans()
    * Description: inner call of transpose()
    */
   void innerTrans();
   void innerCropMul(int, int, const Matrix *, const Matrix *, const int, const int);
public:
   // Constructors
   Matrix();
   Matrix(const Matrix &);
   Matrix(double*, int*);
   ~Matrix();
   Matrix(int hi, int wi = 1, double a = 0.0);
   // Get
   int getHeight() const      { return h; }
   int getRows() const        { return h; }
   int getWidth() const       { return w; }
   int getCols() const        { return w; }
   bool isVector() const      { return (w==1 && h>0); }
   bool isMatrix() const      { return (w>1 && h>0);  }
   // Operator Overloads
   //    Matrix or vector addition
   Matrix operator+(const Matrix) const;
   //    Matrix or vector subtraction
   Matrix operator-(const Matrix) const;
   //    Matrix multiplication or dot product
   Matrix operator*(const Matrix) const;
   //    Matrix or vector scalar
   Matrix operator*(const double) const;
   //    Matrix or vector *= scalar
   Matrix& operator*=(const double) ;
   //    Matrix assignment/overwrite
   Matrix& operator=(const Matrix &);
   //    ostream overload
   friend ostream& operator<<(ostream &, const Matrix &);
   //    element access overload
   double& operator[](int);
   // Special Operations
   //    For 1 row or 1 column "matrices", return length
   double length() const;
   //    Return tier'th 'r' row or 'c' column as vector, skip secondary non 'r'/'c'
   Matrix extract(int tier = 0, char selection = 'c', int secondary = 0);
   // Set
   /*
    * void transpose()
    * Description: transpose
    *    the caller in place
    */
   void transpose();
   /*
    * bool resize(int hi, int wi, double a)
    * Description: Set the size of the matrix
    *    or, if only one integer is given,
    *    the length of the vector. If a double
    *    is given also, fill the matrix with 
    *    that value.
    */
   bool resize(int hi, int wi = 1, double a=0.0);
   /*
    * bool fill(double a)
    * Description: fill the matrix or vector
    *    with the given value, overwriting
    *    previous contents.
    */
   bool fill(double);
   /*
    * void clean()
    * Description: release data[] and set
    *    h and w to 0.
    */
   void clean();
   /*
    * int cropMul(const Matrix *A, const Matrix *B, 
    *    int skipR, int skipC, int myT, int totalT)
    * Description: In the case of QR factorization,
    *    each iteration requires a multiplication
    *    of a vector by a submatrix of decreasing
    *    size. With this, multiply with a submatrix
    *    without allocating space for 2 new matrices.
    *    If totalT != 1, then there is an OMP thread
    *    team working on this object. Only calculate
    *    for a portion of values.
    *    The caller's data will be overwritten if
    *    the OMP thread's load from distributeWork 
    *    includes those array elements.
    * Pre: The caller's width matches the cropped 
    *    argument's height after skipping skipR rows.
    * Post: return a matrix of the caller's height by
    *    the argument's width, less skipC columns.
    */
   int cropMul(const Matrix *, const Matrix *, const int skipR=0, const int skipC=0, int myT=0, int totalT=1);
};

// Given n tasks and p threads, determine the
//    start and stop point for the current thread 
//    identified by me
void distributeWork(int *start, int *stop, int me, int p, int n);
/*
 * Matrix backSubstitute(Matrix *A, Matrix *b)
 * Description: Solve for x in Ax=b given x and b are
 *    vectors and A a matrix.
 * Pre: A and b match dimensions, A is upper triangular.
 * Post: Return a new matrix x and do not altar A. b may
 *    be transposed twice.
 */
Matrix backSubstitute(Matrix *, Matrix *);


