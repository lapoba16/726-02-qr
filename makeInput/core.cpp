#include <iostream>
#include <stdio.h>
#include "../Src/io.h"

using namespace std;

int main() {
   FILE *in;
   in = fopen("in", "r");
   if(in==NULL)
      return -1;
   int h, w;
   int tr;
   tr = fscanf(in, "%i %i", &h, &w);
   double *all = (double*)malloc(sizeof(double) * h * w);
   for(int i=0; i<h; i++) {
      for(int j=0; j<w; j++) {
         tr = fscanf(in, "%lf", &(all[j*h+i]));
      }
   }
   fclose(in);
   for(int i=0; i<h; i++) {
      for(int j=0; j<w; j++) {
         printf("%5.1lf ", all[j*h+i]);
      }
      printf("\n");
   }
   int *sz = (int*)malloc(sizeof(int)*2);
   sz[0] = h;
   sz[1] = w;
   char str[] = "my_out";
   printOut(all, str, sz);
   free(sz);
   free(all);
   return 0;
}
