#include "io.h"

double* readIn(const char* filename, int** sizes) {
   FILE *file = fopen(filename, "rb");
   if(!file) {
      printf("Could not open\n");
      return NULL;
   }
   int m, n;
   int tr;
   tr = fread(&m,sizeof(int),1,file);
   tr = fread(&n,sizeof(int),1,file);
   if(*sizes==NULL)
      *sizes=(int*)malloc(sizeof(int)*2);
   (*sizes)[0] = m;
   (*sizes)[1] = n;
   //printf("File is %i x %i\n", m, n);
   double *ptr = (double*)malloc(sizeof(double)*m*n);
   tr = fread(ptr, sizeof(double), m*n, file);
   fclose(file);
   return ptr;
}

int printOut(double* data, char* filename, int* len) { 
   FILE *file = fopen(filename, "wb");
   int tr;
   if(!file) {
      printf("Could not open to write\n");
      return -1;
   }
   if(data!=NULL) {
      tr = fwrite(len, sizeof(int), 2, file);
      tr = fwrite(data, sizeof(double), len[0]*len[1], file);
      fclose(file);
   } else {
      fclose(file);
      return -1;
   }
   return 0;
}

int cleanQuit() {
   fclose(stdin);
   fclose(stdout);
   fclose(stderr);
   exit(0);
   return -1;
}
