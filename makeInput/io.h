#pragma once
#include <stdio.h>
#include <stdlib.h>

/*
 * double* readIn(const char *fn, int **sz)
 * Description: Given a filename and a pointer
 *    to an integer pointer, allocated at least
 *    2 or not, read the identified binary file
 *    and store its size (the first 8 bytes of
 *    the file) in *sz[] and its contents as a
 *    malloc'd double[sz[0]*sz[1]]
 * Pre: fn exists and is a binary file in the format
 *    [int * 1][int * 1][double * (h*w)]
 * Post: the returned and sz must be free()'d at
 *    some point.
 */
double* readIn(const char*, int**);

/*
 * int printOut(double *d, char *fn, int **sz)
 * Description: Given a dynamic double[*sz[0] * *sz[1]],
 *    print its contents to a binary file *fn
 *    and free(d) and free(*sz)
 * Pre: d and *sz exist and are initiallized, *fn
 *    exists
 * Post: d and *sz point are deallocated and the file
 *    *fn is closed
 */
int printOut(double*, char*, int*);

/*
 * int cleanQuit()
 * Description: Close file pointers and exit()
 * Pre: all allocated memory has been released
 * Post: program ends
 */
int cleanQuit();
