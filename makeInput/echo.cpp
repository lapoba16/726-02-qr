#include <iostream>
#include <stdio.h>
#include "../Src/io.h"

using namespace std;

int main(int argc, char ** argv) {
   FILE *in = fopen((argc>1) ? argv[1] : "my_out", "rb");
   double tmp;
   int h, w;
   int tr;
   tr = fread(&h, sizeof(int), 1, in);
   tr = fread(&w, sizeof(int), 1, in);
   printf("h=%i, w=%i\n", h, w);
   double all[h*w];
   tr = fread(all, sizeof(double), h*w, in);
   fclose(in);
   tr = 0;
   for(int i=0; i<h; i++) {
      for(int j=0; j<w; j++) {
         if(++tr%5==0)
            printf("\n");
         printf("%8.5lf ", all[j*h+i]);
      }
   }
   printf("\n");
   return 0;
}
